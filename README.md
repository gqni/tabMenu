## Vue实现选项卡

```
<!DOCTYPE html>
<html>
 
	<head>
		<meta charset="utf-8" />		
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">		
		<meta name="viewport" content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=no,width=device-width">
		<meta name="apple-mobile-web-app-title" content="Vue选项卡">
		<title>Vue实现选项卡</title>
		<script type="text/javascript" src="js/vue.js"></script>
	</head>
	<style>
	* {padding: 0;margin: 0;}
	.box {margin: 0 auto;}
	.tabs {display: flex;}
	.tabs li {flex:1;list-style: none;margin-right:1px;height: 50px;line-height: 50px;text-align: center;background-color: #ccc;color: #fff;}
	.tabs .active {background-color: #5597B4;transition: .3s;}
	.hide{display: none;}
	</style>
 
	<body>
		<div id="app" class="box">
			<ul class="tabs">
				<li v-for="(tab,index) in tabsName" @click="tabsSwitch(index)" v-bind:class="{active:tab.isActive}">{{tab.name}}</li>
			</ul>
 
			<div class="cards">
				<div class="tab-card " >这里是HTML教程</div>
				<div class="tab-card hide" >欢迎来到CSS模块</div>
				<div class="tab-card hide" >嗨，这里是Vue</div>
			</div>
		</div>
	</body>
 
	<script>
		var app = new Vue({
			el: "#app",
			data: {
				tabsName: [{
					name: "HTML",
					isActive: true
				}, {
					name: "CSS",
					isActive: false
				}, {
					name: "Vue",
					isActive: false
				}],
				active: false
			},
			methods: {
				tabsSwitch(index) {
					var tab = document.querySelectorAll(".tab-card"),len = tab.length;
					for(var i = 0; i < len; i++) {
						 tab[i].style.display = "none";
						 this.tabsName[i].isActive = false;
					}
					this.tabsName[index].isActive = true;
					tab[index].style.display = "block";
				}
			}
		})
	</script>
 
</html>

```

![输入图片说明](https://images.gitee.com/uploads/images/2018/0920/164717_892ba62f_2011549.png "屏幕截图.png")